# NodeJs-Pipeline

# GitLab NodeJs Pipeline 

---

* Step 1: Register/Login to [gitlab](https://gitlab.com/users/sign_in)

* Step 2: [Create a gitlab project](https://gitlab.com/projects/new)

  * Click to `import project` icon

  * Click to `Repo by URL`

  * Fill `https://gitlab.com/ti1akt/nodejs-pipeline.git` as the `Git repository URL`, specify a project name and click `create the project` button

```bash
https://gitlab.com/ti1akt/nodejs-pipeline.git
```

* Step 3: Once the repository has been imported, all files should be visible in the project, and the pipeline will be automatically triggered

* Step 4: `.gitlab-ci.yml` is the file that is configured to build the docker image and run specified scans.

  * `nodejsscan_sast` runs a [NodeJsScan](https://github.com/ajinabraham/nodejsscan) SAST scan and `Fails` if there are findings. 

    * `allow_failure: true` option ignores the failure and continues executing the pipeline.

  * `npmaudit_dependency_scanning` runs a [npm audit](https://docs.npmjs.com/cli/audit) SCA scan on the dependencies

  * [`whispers_secrets_scan`](https://github.com/Skyscanner/whispers) looks for hardcoded credentials and sensitive information in the repository

* Step 5: Select `CI/CD` on the sidebar to view the pipeline.

* Step 6: Wait for the pipeline to execute and observe the results
